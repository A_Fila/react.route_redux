//import React from 'react';
import './App.css';

import Header from './components/Header';
import Footer from './components/Footer';
import './bootstrap.min.css'
//import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Pages/Home';
import Login from './Pages/Login';
import Register from './Pages/Register';
import { Page404 } from './Pages/Page404';
import { Logout } from './Pages/Logout';
import { useAppSelector } from './hooks/redux';
//import { setupStore } from './store/store';
//import { Provider } from 'react-redux'


function App() {
    //const isAuth = useAppSelector(state => state.userReducer.authInx  !== -1);
    return (
        <BrowserRouter>

            <Header />
            <main>
                <Container>
                <Routes>
                        <Route path="/" element={<Home />} />
                        
                        <Route path="/Login" element={<Login />} />
                        <Route path="/Register" element={<Register />} />
                        <Route path="/Logout" element={<Logout />} />

                        <Route path="*" element={<Page404 />} />
                 </Routes>  
                </Container>
            </main>
                <Footer />

            </BrowserRouter>
  );
}

export default App;