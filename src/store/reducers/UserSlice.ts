import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { IUser } from '../../models/IUser'

interface UserState {
    users: IUser[];
    authInx: number;
    error: string;    
}

const initialState: UserState = {
    users: [],
    authInx: -1,
    error: ''
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        registry(state, action: PayloadAction<IUser>) {
            const inx = findUser(state, action.payload);
            console.log('createSlice.registry(): inx=', inx);

            if (inx >= 0)
                state.error = `User with ${action.payload.email} already registered!`;
            else
            {                
                state.users.push(action.payload)
                state.error = '';
               // useNavigate('');
            }
        },
        login(state, action: PayloadAction<IUser>) {
            console.log('createSlice.login state=', state)
            state.authInx = loginUser(state, action.payload);
            state.error = (state.authInx === -1) ? 'Invalid username or password'
                                                 : '';    
            console.log('createSlice.login(): authInx=', state.authInx);                                                 
        },
        logout(state, action: PayloadAction<string>) {
            console.log(`createSlice.logout() user with inx=${state.authInx} has been logged out at ${action.payload}`);
            state.authInx = -1;
            
        }
    }
})
const findUser = (state: UserState, newUser: IUser) => {
    return state.users.findIndex(u => u.email.trim().toLowerCase() ===
            newUser.email.trim().toLowerCase());    
}

const loginUser = (state: UserState, newUser: IUser) => {

    //find user in application storage
    const inx = findUser(state, newUser);

    if (inx !== -1)
        if(state.users[inx].pass !== newUser.pass) //check pass
            return -1;

    return inx;
}

//export const userSlice = createSlice({
//    name: 'user',
//    initialState,
//    reducers: {
//        increment(state, action: PayloadAction<number>) {
//            state.count += action.payload;
//        }
//    }
//})

export default userSlice.reducer;