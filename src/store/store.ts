//import { combineReducers, configureStore } from "@reduxjs/toolkit";
//import userReducer from './reducers/UserSlice'

//const rootReducer = combineReducers({
//    userReducer
//});

//export const setupStore = () => {
//    return configureStore({ reducer: rootReducer, })
//}

//export type RootState = ReturnType<typeof rootReducer>
//export type AppStore = ReturnType<typeof setupStore>
//export type AppDispatch = AppStore['dispatch']

import { configureStore } from '@reduxjs/toolkit';
//import { bookSlice } from './bookSlice';
import { userSlice } from './reducers/UserSlice'

export const store = configureStore({
    reducer: {
        userReducer: userSlice.reducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch; 