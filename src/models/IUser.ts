export interface IUser {
    id: number;
    name: string;
    lastName: string;
    pass: string;
    email: string;
}