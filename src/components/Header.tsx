import { Container, Nav, Navbar } from "react-bootstrap";
import { useAppSelector } from "../hooks/redux";
//import { Link } from "react-router-dom";
import { Link, useLocation } from 'react-router-dom';

const Header = () => {
    const state =  useAppSelector(state => state.userReducer);
    const authInx = state.authInx;
   
    return( 
        <>            
            <Navbar expand="lg" bg="dark" variant="dark" collapseOnSelect>
                <Container>
                    <Navbar.Brand to="/" as={Link}>HW 6</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ms-auto">
                            {
                                authInx === -1 ?
                                    <>
                                        <Nav.Link to="/login" as={Link}>Login</Nav.Link>
                                        <Nav.Link to="/register" as={Link}>Register</Nav.Link>
                                    </>
                                    :
                                    <>
                                        <Nav.Link to="/lk" as={Link} >{state.users[authInx].name} </Nav.Link>
                                        <Nav.Link to="/Logout" as={Link}>Logout</Nav.Link>
                                    </>
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

        
        </>
    )
}

export default Header;