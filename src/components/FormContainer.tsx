
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

interface Props {
    children?: React.ReactNode;
}

const FormContainer: React.FC<Props> = ({ children }) => {
    return (
        <Container className="py-3">
            <Row className="justify-content-md-center">
                <Col xs={12} md={5}>{children}</Col>
            </Row>
        </Container>
    )
}


export default FormContainer;