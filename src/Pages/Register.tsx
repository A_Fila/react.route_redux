import { Alert, Button, Form } from "react-bootstrap";
import FormContainer from "../components/FormContainer";
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { userSlice } from "../store/reducers/UserSlice";
import { useNavigate } from "react-router-dom";

const PASS_MIN_LEN = 3;

const Register = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const state = useAppSelector(state => state.userReducer)
   
    const registry = userSlice.actions.registry;

    const [validated, setValidated] = useState(false);
    const [isSubmitted, setIsSubmitted] = useState(false);

    console.log('validated=', validated)

    const [formData, setFormData] = useState({
        firstName: '',
        lastName: '',
        pass: '',
        confirmPass: '',
        email: ''
    })
    
    //const [firstName, setfirstName] = useState('');
    //const [lastName, setlastName] = useState('');
    //const [email, setEmail] = useState('');
    //const [pass, setPass] = useState('');

    const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
        const form = event.currentTarget;
        //console.log('form', form);
        event.preventDefault();
        setIsSubmitted(false);

        if (form.checkValidity() === false) {
            //event.preventDefault();
            event.stopPropagation();
        }
        else {
            const user = {
                id: state.users.length,
                name: formData.firstName,
                lastName: formData.lastName,
                pass: formData.pass,
                email: formData.email,
            }
            //console.log('Registry user', user);
            console.log('before disp', state.users.length)

            dispatch(registry(user));  
            setIsSubmitted(true);
            console.log('after submit', state.users.length)            
        }

        setValidated(true); 
    }

    const formElementChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) =>{
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value
        });
    }

    return (
        <FormContainer>
            <h1>Register</h1>
            <Form onSubmit={submitHandler} noValidate validated={validated}>
                <Form.Group className="mb-3" controlId="fgFirstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter your firts name"
                        name= "firstName"
                        value={formData.firstName}
                        onChange={formElementChangeHandler}
                        required
                        isInvalid={ validated &&
                                    !/^[a-zA-Zа-яА-Я]+$/.test(formData.firstName)
                        }
                    />
                    <Form.Control.Feedback type="invalid">
                        Please enter a valid username
                        (characters only).
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="fgLastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter your last name"
                        name ="lastName"
                        value={formData.lastName}
                        onChange={formElementChangeHandler}
                    />
                </Form.Group>

                <Form.Group className="mb-3 my-3" controlId="fgEmail" >
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="text" placeholder="Enter your email"
                        name="email"
                        value={formData.email}
                        onChange={formElementChangeHandler}
                        required
                        isInvalid={validated &&
                            (!/^\S+@\S+\.\S+$/.test(formData.email) || state.error !== '')
                        }
                    />
                    <Form.Control.Feedback type="invalid">{state.error || 'Please enter a valid email address.'}</Form.Control.Feedback>
                    <Form.Text className="text-muted"> We'll never share your email with anyone else.</Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="fgPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password"
                        name="pass"
                        value={formData.pass}
                        onChange={formElementChangeHandler}
                        required
                        minLength={PASS_MIN_LEN}
                        isInvalid={ validated &&
                                    formData.pass.length < PASS_MIN_LEN
                        }
                    />
                    <Form.Control.Feedback type="invalid">
                        Password must be at least {PASS_MIN_LEN} characters long.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="fgConfirmPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password"
                        name="confirmPass"
                        value={formData.confirmPass}
                        onChange={formElementChangeHandler}
                        required
                        pattern={formData.pass}
                        minLength={PASS_MIN_LEN}
                        isInvalid={ validated &&
                                    formData.pass !== formData.confirmPass
                        }
                    />
                    <Form.Control.Feedback type="invalid">
                        Passwords do not match.
                    </Form.Control.Feedback>
                </Form.Group>
                {/*
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                */}

                    <Button variant="primary" type="submit">
                        Submit
                </Button>
               
                <Alert
                    variant="success"
                    className={isSubmitted && state.error === ''
                        ? "d-block mt-3 mb-0"
                        : "d-none"}
                >
                    User {formData.firstName} has been succsesfully registered!
                    <div className="small fw-bold mt-2 pt-1 mb-0"
                        style={{
                            width: '100%',                           
                            textAlign: 'center',
                            fontSize: '1.5em',
                        }}>
                        <a className="link-success"
                            onClick={() => navigate('/Login')}
                            style={{ cursor: 'pointer' }}>
                            Login
                        </a>
                    </div>
                        
                </Alert>
                
            </Form>
        </FormContainer>
    )
}

export default Register;