import { Navigate  } from "react-router-dom";
import { useAppDispatch } from "../hooks/redux"
import { userSlice } from "../store/reducers/UserSlice";
import { useEffect } from "react";

export function Logout() {

    const logout = userSlice.actions.logout;    
    const dispatch = useAppDispatch();
    
    useEffect(() => {
        dispatch(logout(new Date().toISOString()));
    }); //, []


    return <Navigate to='/' />;    
}