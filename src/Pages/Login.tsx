import { Button, Form } from "react-bootstrap";
import FormContainer from "../components/FormContainer";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { userSlice } from "../store/reducers/UserSlice";
import { useNavigate } from "react-router-dom";

//const navigate = useNavigate();

const Login = () => {
    //const [email, setEmail] = useState('');
    //const [pass, setPass] = useState('');
    const navigate = useNavigate();

    const state = useAppSelector(state => state.userReducer)
    console.log('Login() state=', state);
     

    const login = userSlice.actions.login;
    const dispatch = useAppDispatch();

    const [validated, setValidated] = useState(false);
    const [formData, setFormData] = useState({
        email: '',
        pass: ''
    })

    const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
        const form = event.currentTarget;
        event.preventDefault();
        //console.log('submitted');
        //console.log('email', email);
        //console.log('password', pass);

        if (form.checkValidity() === false) {
            //event.preventDefault();
            event.stopPropagation();
        }
        else {
            const user = {
                id: -1,
                name: '',
                lastName: '',
                pass: formData.pass,
                email: formData.email,
            }
            //console.log('Registry user', user);
            //console.log('before disp', state.users.length)

            dispatch(login(user));

           //setIsSubmitted(true);
            console.log('after submit', state.users.length)
            console.log('after submit', state.authInx)

        }
 
        setValidated(true); 

    }

    const formElementChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value
        });
    }

    useEffect(() => {
        if (state.authInx !== -1) {
            //navigation.navigate('ProfileScreen');
            navigate('/');
        }
    }, [state.authInx]);
    //if (state.authInx !== -1)
    //    navigate('/');

    return (
        <FormContainer>
            <h1>Login</h1>
            <Form onSubmit={submitHandler} noValidate validated={validated}>
                <Form.Group className="mb-3 my-3" controlId="fgEmail" >
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="text" placeholder="Enter your email"
                        name="email"
                        value={formData.email}
                        onChange={formElementChangeHandler}
                        required
                        isInvalid={validated &&
                            (!/^\S+@\S+\.\S+$/.test(formData.email) || state.error !== '')
                        }
                    />
                    <Form.Control.Feedback type="invalid">{state.error || 'Please enter a valid email address.'}</Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="fgPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password"
                        name="pass"
                        value={formData.pass}
                        onChange={formElementChangeHandler}
                        required
                        isInvalid={validated}
                    />
                </Form.Group>
                {/*
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                */ }
                <Button variant="primary" type="submit">
                    Submit
                </Button>
                <p className="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <a href="/register"
                    className="link-danger">Register</a></p>


            </Form>
        </FormContainer>
    )
}

export default Login;