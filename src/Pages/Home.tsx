//import { Col, Container, Row } from "react-bootstrap";
import React from 'react';
const Home = () => {
    return (
        <>
            <div className="learning-near__item" >
                <h2 className="learning-near__header">Домашнее задание</h2>
                <div className="text text_p-small text_default learning-markdown js-learning-markdown"><p>Роутинг и управление стейтом с React</p> </div>
                <div className="text text_p-small text_default text_bold">Цель:</div>
                <div className="text text_p-small text_default learning-markdown js-learning-markdown">
                    <p>Научиться писать сложный фронтенд с роутингом и управлением стейтом
                        <br />Разобраться с подключением сторонних плагинов и UI компонентов</p>
                </div>
                <br />
                <div className="text text_p-small text_default text_bold">Описание/Пошаговая инструкция выполнения домашнего задания:</div>
                <div className="text text_p-small text_default learning-markdown js-learning-markdown">
                    <ol>
                        <li>Установить react-router;</li>
                        <li>Добавить отдельные компоненты страниц - <a href="/Login">Login</a> /
                            <a href="/Register"> Register</a> / <a href="/">HomePage</a> / <a href="/404">404</a> </li>
                        <li>Добавить стейт-менеджемент с Redux;</li>
                        <li>Найти возможное дублирование кода и применить HOC паттерн.</li>
                    </ol>
                </div>
                <br />
                <div className="text text_p-small text_default text_bold">Критерии оценки:</div>
                <div className="text text_p-small text_default learning-markdown js-learning-markdown"><p>4 балла: В React проект добавлен react-router;
                    <br />2 балла: В React проект добавлен Redux;
                    <br />2 балла: В React проект добавлена библиотека UI компонентов (Bootstrap / material design);
                    <br />1 балл: Сделано 2 CodeReview;<br />1 балл: CodeStyle, грамотная архитектура, все замечания проверяющего исправлены.<br />Минимальный проходной балл: 8</p>
                </div>
                <br />
                <div className="text text_p-small text_default">Рекомендуем сдать до: </div>
                <div className="text text_p-small text_default">
                    Статус: не сдано
                    <a href="#/homework-chat/25627/" className="button learning-near__hw-button" title=" Отправить ДЗ на проверку">
                        Отправить ДЗ на проверку
                    </a>
                </div>
            </div>
        </>
    )
}

export default Home;